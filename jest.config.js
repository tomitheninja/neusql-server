module.exports = {
  roots: ['<rootDir>/src'],
  testMatch: ['**/__tests__/**/*.+(ts|js)', '**/?(*.)+(spec|test).+(ts|js)'],
  transform: {
    '^.+\\.(ts)?$': 'ts-jest',
    '\\.(gql|graphql)$': 'jest-transform-graphql',
  },
  moduleNameMapper: {
    '~(.*)$': '<rootDir>/src/$1',
  },
  coveragePathIgnorePatterns: [
    '/node_modules/',
    '<rootDir>/src/routes/graphql/schema.graphql',
  ],
}
