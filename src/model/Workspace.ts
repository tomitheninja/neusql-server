/// note: Promisify will not work with sqlite3 callbacks
import { join } from 'path'
import fs from 'fs'
import { verbose as sqlite3Factory, Database } from 'sqlite3'
import debugFactory from 'debug'

import { WorkspaceID, createWorkspace, runSQLinWorkspace } from '~/db/workspace'

const log = debugFactory('app:model:workspace')
const sqlite3 = sqlite3Factory()

const DB_DATA_DIR = join(__dirname, '/.data/')

export interface WorkspaceProps {
  workspaceID: WorkspaceID
}

export default class Workspace {
  private readonly db: Database
  private readonly dbPath: string
  public readonly workspaceID: WorkspaceID
  constructor(workspaceID: WorkspaceID) {
    log('Load workspace %d', workspaceID)
    if (!fs.existsSync(DB_DATA_DIR)) {
      fs.mkdirSync(DB_DATA_DIR)
    }
    Workspace.validateWorkspaceID(workspaceID)
    this.workspaceID = workspaceID
    this.dbPath = join(DB_DATA_DIR, '/workspace_' + workspaceID + '.db')
    this.db = new sqlite3.Database(this.dbPath)
    log('Loaded workspace %s', this.workspaceID)
  }

  async createDatabase() {
    return createWorkspace(this.db)
  }

  async dropDatabase() {
    log('Drop workspace.database for %s', this.workspaceID)
    return new Promise<void>(async (resolve, reject) => {
      fs.unlink(this.dbPath, async err => {
        if (err) reject(err)
        else {
          log('Dropped workspace.database for %s', this.workspaceID)
          resolve()
        }
      })
    })
  }

  async runSQL(sql: string) {
    return runSQLinWorkspace(this.db, {
      sql,
      workspaceID: this.workspaceID,
    })
  }

  private static validateWorkspaceID(workspaceID: WorkspaceID) {
    if (workspaceID < 1 || !Number.isInteger(workspaceID)) {
      throw new Error(workspaceID + ' is not a valid workspace id')
    }
  }
}
