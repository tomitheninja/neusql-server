import debugFactory from 'debug'

import db from '~/db/index'
import Workspace from '~/model/Workspace'

import {
  createProject,
  getProjectOwner,
  getProjectCreatedAt,
  getProjectName,
  setProjectName,
  CreateProjectProps,
  SetProjectNameProps,
  ProjectID,
} from '../db/project'

const log = debugFactory('app:model:Project')

export default class Project {
  static async create(props: CreateProjectProps) {
    log('Create project %s', props.projectName)
    const id = await createProject(db, props)
    log('Created project %s', props.projectName)
    log('Create workspace for %d', id)
    new Workspace(id).createDatabase()
    log('Created workspace for %d', id)
    return this.open(id)
  }

  static async open(id: ProjectID) {
    return new Project(id)
  }

  private constructor(private id: ProjectID) {
    log('Open project %s', id)
    if (!Number.isInteger(id)) {
      throw new Error(id + ' is not a valid project ID')
    }
    log('Opened project %d', id)
  }

  get workspace() {
    return new Workspace(this.id)
  }

  async getOwner() {
    return getProjectOwner(db, { projectID: this.id })
  }

  async getCreatedAt() {
    return getProjectCreatedAt(db, { projectID: this.id })
  }

  async getName() {
    return getProjectName(db, { projectID: this.id })
  }

  async setName(props: Omit<SetProjectNameProps, 'projectID'>) {
    return setProjectName(db, { ...props, projectID: this.id })
  }
}
