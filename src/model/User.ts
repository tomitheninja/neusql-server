import debugFactory from 'debug'

import db from '../db/index'
import {
  createUser,
  getUserCreatedAt,
  getUserName,
  setUserName,
  comparePassword,
  setUserPassword,
  CreateUserProps,
  SetUserNameProps,
  SetUserPasswordProps,
  UserID,
  ComparePasswordProps,
} from '../db/user'

const log = debugFactory('app:model:User')

export default class User {
  static async create(props: CreateUserProps) {
    log('Create user %s', props.userName)
    const id = await createUser(db, props)
    log('Created user %s created with id=%d', props.userName, id)
    return this.open(id)
  }

  static async open(id: UserID) {
    return new User(id)
  }

  private constructor(private id: UserID) {
    log('Open user %d', id)
    if (!Number.isInteger(id)) {
      throw new Error(id + ' is not a valid user ID')
    }
    log('Opened user %d', id)
  }

  async getCreatedAt() {
    return getUserCreatedAt(db, { userID: this.id })
  }

  async getName() {
    return getUserName(db, { userID: this.id })
  }

  async setName(props: Omit<SetUserNameProps, 'userID'>) {
    if (props.userName.length < 5) {
      throw new Error('Username should be at least 5 characters')
    }
    return setUserName(db, { ...props, userID: this.id })
  }

  async setPassword({ password }: Omit<SetUserPasswordProps, 'userID'>) {
    if (password.length < 10) {
      throw new Error('Password should be at least 10 characters')
    }
    return setUserPassword(db, { password, userID: this.id })
  }

  async comparePassword({ password }: Omit<ComparePasswordProps, 'userID'>) {
    return comparePassword(db, { password, userID: this.id })
  }
}
