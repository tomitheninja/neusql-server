import { join } from 'path'
import { verbose as sqlite3Factory } from 'sqlite3'
import debugFactory from 'debug'

import { NODE_ENV, DATA_PATH } from '~/config'
import { initializeUserTable } from './user'
import { initializeProjectTable } from './project'

const log = debugFactory('app:db')

const DB_PATH = DATA_PATH ? join(DATA_PATH, NODE_ENV + '.db') : ':memory:'
log('Load database ' + DB_PATH)
export const db = new (sqlite3Factory().Database)(DB_PATH)

let initializationPromise: Promise<void> | null = null
export const waitForDatabaseInitialization = () => {
  initializationPromise =
    initializationPromise ||
    new Promise(async (resolve, reject) => {
      db.serialize(async () => {
        try {
          log('Initialize database')
          await initializeUserTable(db)
          await initializeProjectTable(db)
          log('Database initialized')
          resolve()
        } catch (err) {
          reject(err)
        }
      })
    })
  return initializationPromise
}

// Start initialization when this file loads
waitForDatabaseInitialization()

export default db
