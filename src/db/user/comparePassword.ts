import { Database } from 'sqlite3'
import debugFactory from 'debug'
import { compare } from 'bcrypt'

import { UserDataStructure } from './types'

const log = debugFactory('app:db:user:comparePassword')

const sql = /* sql */ `
  SELECT password
  FROM user
  WHERE userID = ?;`

type QueryResult = Pick<UserDataStructure, 'password'>
export type ComparePasswordProps = Pick<
  UserDataStructure,
  'userID' | 'password'
>

export type ComparePassword = (
  db: Database,
  props: ComparePasswordProps
) => Promise<boolean>

export const comparePassword: ComparePassword = async function(db, props) {
  const { userID, password: passwordCandidate } = props
  return new Promise(async (resolve, reject) => {
    log('Compare user.password for %d', userID)
    db.get(sql, [userID], async function(err, { password }: QueryResult) {
      if (err) reject(err)
      else {
        log('Got user.password for %d', userID)

        const isMatches = await compare(passwordCandidate, password)
        log(`Password ${isMatches ? '' : 'not '}matches for %d`, userID)
        resolve(isMatches)
      }
    })
  })
}
