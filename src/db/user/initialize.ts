import { Database } from 'sqlite3'
import debugFactory from 'debug'

import serialize from '../serialize'

const log = debugFactory('app:db:user:initialize')

const sql = /* sql */ `
  CREATE TABLE IF NOT EXISTS user (
    userID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    createdAt TIME NOT NULL,
    userName TEXT NOT NULL,
    password TEXT NOT NULL
  );`

export const initializeUserTable = async function(db: Database) {
  await serialize(db)
  return new Promise<void>(async (resolve, reject) => {
    log('Initialize table user')
    db.run(sql, async err => {
      if (err) reject(err)
      else {
        log('Table user Initialized')
        resolve()
      }
    })
  })
}
