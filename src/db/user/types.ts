export interface UserDataStructure {
  userID: number
  userName: string
  createdAt: string
  password: string
}

export type UserID = UserDataStructure['userID']
