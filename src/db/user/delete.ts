import { Database } from 'sqlite3'
import debugFactory from 'debug'

import serialize from '../serialize'
import { UserDataStructure } from './types'

const log = debugFactory('app:db:user:delete')

const sql = /* sql */ `
  DELETE FROM user
  WHERE userID = ?;`

export type DeleteUserProps = Pick<UserDataStructure, 'userID'>

export type DeleteUser = (
  db: Database,
  props: DeleteUserProps
) => Promise<boolean>

export const deleteUser: DeleteUser = async function(db, { userID }) {
  return new Promise(async (resolve, reject) => {
    log('Delete user %s', userID)
    await serialize(db)
    db.run(sql, [userID], async function(err) {
      if (err) reject(err)
      else {
        log('User %s deleted', userID)
        resolve(this.changes === 1)
      }
    })
  })
}
