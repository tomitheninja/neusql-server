import { Database } from 'sqlite3'
import debugFactory from 'debug'
import { genSalt, hash } from 'bcrypt'

import serialize from '~/db/serialize'
import { UserDataStructure } from './types'
import { BCRYPT_ROUNDS } from '~/config'

const log = debugFactory('app:db:user:create')

const sql = /* sql */ `
  INSERT INTO user (createdAt, userName, password)
  VALUES ($createdAt, $userName, $password);`

export type CreateUserProps = Pick<UserDataStructure, 'userName' | 'password'>

export type CreateUser = (
  db: Database,
  props: CreateUserProps
) => Promise<UserDataStructure['userID']>

export const createUser: CreateUser = async function(db, arg) {
  const { userName, password } = arg
  return new Promise(async (resolve, reject) => {
    log('Create user %s', userName)
    await serialize(db)
    db.run(
      sql,
      {
        $createdAt: new Date(),
        $userName: userName,
        $password: await hash(password, await genSalt(BCRYPT_ROUNDS)),
      },
      async function(err) {
        if (err) reject(err)
        else {
          log('User %s created with id=%d', userName, this.lastID)
          resolve(this.lastID)
        }
      }
    )
  })
}
