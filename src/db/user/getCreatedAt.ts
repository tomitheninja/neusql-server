import { Database } from 'sqlite3'
import debugFactory from 'debug'

import { UserDataStructure } from './types'

const log = debugFactory('app:db:user:getCreatedAt')

const sql = /* sql */ `
  SELECT createdAt
  FROM user
  WHERE userID = ?;`

type QueryResult = Pick<UserDataStructure, 'createdAt'>

export type GetUserCreatedAtProps = Pick<UserDataStructure, 'userID'>

export type GetUserCreatedAt = (
  db: Database,
  props: GetUserCreatedAtProps
) => Promise<Date>

export const getUserCreatedAt: GetUserCreatedAt = async function(
  db,
  { userID }
) {
  return new Promise(async (resolve, reject) => {
    log('Get user.createdAt for %d', userID)
    db.get(sql, [userID], async function(err, row: QueryResult) {
      const timestamp = Number.parseInt(row.createdAt, 10)
      if (err) {
        reject(err)
      } else if (Number.isNaN(timestamp)) {
        reject(new Error('Invalid Date in database'))
      } else {
        log('Got user.createdAt for %d', userID)
        resolve(new Date(timestamp))
      }
    })
  })
}
