import { Database } from 'sqlite3'
import debugFactory from 'debug'

import { UserDataStructure } from './types'

const log = debugFactory('app:db:user:getName')

const sql = /* sql */ `
  SELECT userName
  FROM user
  WHERE userID = ?;`

type QueryResult = Pick<UserDataStructure, 'userName'>
export type GetUserNameProps = Pick<UserDataStructure, 'userID'>

export type GetUserName = (
  db: Database,
  props: GetUserNameProps
) => Promise<UserDataStructure['userName']>

export const getUserName: GetUserName = async function(db, { userID }) {
  return new Promise<UserDataStructure['userName']>(async (resolve, reject) => {
    log('Get user.name for %d', userID)
    db.get(sql, [userID], async function(err, row: QueryResult) {
      if (err) reject(err)
      else {
        log('Got user.name for %d', userID)
        resolve(row.userName)
      }
    })
  })
}
