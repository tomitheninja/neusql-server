import { Database } from 'sqlite3'
import debugFactory from 'debug'
import { genSalt, hash } from 'bcrypt'

import serialize from '../serialize'
import { UserDataStructure } from './types'
import { BCRYPT_ROUNDS } from '~/config'

const log = debugFactory('app:db:user:setPassword')

const sql = /* sql */ `
  UPDATE user
  SET password = $password
  WHERE userID = $userID;`

export type SetUserPasswordProps = Pick<
  UserDataStructure,
  'userID' | 'password'
>

export type SetUserPassword = (
  db: Database,
  props: SetUserPasswordProps
) => Promise<void>

export const setUserPassword: SetUserPassword = async function(db, props) {
  const { userID, password } = props
  return new Promise(async (resolve, reject) => {
    log('Set user.password for %d', userID)
    await serialize(db)
    db.run(
      sql,
      {
        $userID: userID,
        $password: await hash(password, await genSalt(BCRYPT_ROUNDS)),
      },
      async function(err) {
        if (err) reject(err)
        else {
          log('Set user.password for %d', userID)
          resolve()
        }
      }
    )
  })
}
