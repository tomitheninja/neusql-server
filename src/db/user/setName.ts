import { Database } from 'sqlite3'
import debugFactory from 'debug'

import serialize from '../serialize'
import { UserDataStructure } from './types'

const log = debugFactory('app:db:user:setName')

const sql = /* sql */ `
  UPDATE user
  SET userName = $userName
  WHERE userID = $userID;`

export type SetUserNameProps = Pick<UserDataStructure, 'userID' | 'userName'>

export type SetUserName = (
  db: Database,
  props: SetUserNameProps
) => Promise<void>

export const setUserName: SetUserName = async function(
  db,
  { userID, userName }
) {
  return new Promise(async (resolve, reject) => {
    log('Set user.name for %d', userID)
    await serialize(db)
    db.run(
      sql,
      {
        $userID: userID,
        $userName: userName,
      },
      async function(err) {
        if (err) reject(err)
        else {
          log('Set user.name for %d', userID)
          resolve()
        }
      }
    )
  })
}
