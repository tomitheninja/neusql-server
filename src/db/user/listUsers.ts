import { Database } from 'sqlite3'
import debugFactory from 'debug'

import { UserDataStructure } from './types'

const log = debugFactory('app:db:user:listUsers')

const sql = /* sql */ `
  SELECT userID
  FROM user;`

type QueryResult = Pick<UserDataStructure, 'userID'>[]
export type ListUsers = (db: Database) => Promise<QueryResult>

export const listUsers: ListUsers = async function(db) {
  return new Promise(async (resolve, reject) => {
    log('Get user list')
    db.all(sql, async function(err, rows: QueryResult) {
      if (err) reject(err)
      else {
        log('Got user list')
        resolve(rows)
      }
    })
  })
}
