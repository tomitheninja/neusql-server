import { Database } from 'sqlite3'
import debugFactory from 'debug'

import serialize from '../serialize'
import { ProjectDataStructure } from './types'

const log = debugFactory('app:db:project:create')

const sql = /* sql */ `
  INSERT INTO project (projectName, createdAt, owner)
  VALUES ($projectName, $createdAt, $owner);`

export type CreateProjectProps = Pick<
  ProjectDataStructure,
  'projectName' | 'owner'
>

export type CreateProjectResult = ProjectDataStructure['projectID']
export type CreateProject = (
  db: Database,
  props: CreateProjectProps
) => Promise<CreateProjectResult>

export const createProject: CreateProject = async function(
  db,
  { projectName, owner }
) {
  return new Promise(async (resolve, reject) => {
    log('Create project name = "%s"', projectName)
    await serialize(db)
    db.run(
      sql,
      {
        $projectName: projectName,
        $createdAt: new Date(),
        $owner: owner,
      },
      async function(err) {
        if (err) reject(err)
        else {
          log('Project "%s" created with id=%d', projectName, this.lastID)
          resolve(this.lastID)
        }
      }
    )
  })
}
