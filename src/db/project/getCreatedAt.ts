import { Database } from 'sqlite3'
import debugFactory from 'debug'

import { ProjectDataStructure } from './types'

const log = debugFactory('app:db:project:getCreatedAt')

const sql = /* sql */ `
  SELECT createdAt
  FROM project
  WHERE projectID = ?;`

type QueryResult = Pick<ProjectDataStructure, 'createdAt'>
export type GetProjectCreatedAtProps = Pick<ProjectDataStructure, 'projectID'>
export type GetProjectCreatedAt = (
  db: Database,
  props: GetProjectCreatedAtProps
) => Promise<Date>

export const getProjectCreatedAt: GetProjectCreatedAt = async function(
  db,
  { projectID }
) {
  return new Promise<Date>(async (resolve, reject) => {
    log('Get project.createdAt for %d', projectID)
    db.get(sql, [projectID], async function(err, row: QueryResult) {
      const timestamp = Number.parseInt(row.createdAt, 10)
      if (err) {
        reject(err)
      } else if (Number.isNaN(timestamp)) {
        reject(new Error('Invalid Date in database'))
      } else {
        log('Got project.createdAt for %d', projectID)
        resolve(new Date(timestamp))
      }
    })
  })
}
