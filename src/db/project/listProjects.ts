import { Database } from 'sqlite3'
import debugFactory from 'debug'

import { ProjectDataStructure } from './types'

const log = debugFactory('app:db:project:listProjects')

const sql = /* sql */ `
  SELECT projectID
  FROM project;`

type QueryResult = Pick<ProjectDataStructure, 'projectID'>[]
export type ListProjectsReturn = QueryResult
export type ListProjects = (db: Database) => Promise<ListProjectsReturn>

export const listProjects: ListProjects = async function(db) {
  return new Promise(async (resolve, reject) => {
    log('Get project list')
    db.all(sql, async function(err, rows: QueryResult) {
      if (err) reject(err)
      else {
        log('Got project list')
        resolve(rows)
      }
    })
  })
}
