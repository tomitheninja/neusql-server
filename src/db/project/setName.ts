import { Database } from 'sqlite3'
import debugFactory from 'debug'

import serialize from '../serialize'
import { ProjectDataStructure } from './types'

const log = debugFactory('app:db:project:setName')

const sql = /* sql */ `
  UPDATE project
  SET projectName = $projectName
  WHERE projectID = $projectID;`

export type SetProjectNameProps = Pick<
  ProjectDataStructure,
  'projectID' | 'projectName'
>

export type SetProjectName = (
  db: Database,
  props: SetProjectNameProps
) => Promise<void>

export const setProjectName: SetProjectName = async function(
  db,
  { projectID, projectName }
) {
  return new Promise(async (resolve, reject) => {
    log('Set project.name for %d', projectID)
    await serialize(db)
    db.run(
      sql,
      {
        $projectID: projectID,
        $projectName: projectName,
      },
      async function(err) {
        if (err) reject(err)
        else {
          log('Set project.name for %d', projectID)
          resolve()
        }
      }
    )
  })
}
