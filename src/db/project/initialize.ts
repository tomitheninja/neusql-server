import { Database } from 'sqlite3'
import debugFactory from 'debug'

import serialize from '../serialize'

const log = debugFactory('app:db:project:initialize')

const sql = /* sql */ `
  CREATE TABLE IF NOT EXISTS project (
    projectID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    projectName TEXT NOT NULL ,
    createdAt TIME,
    owner INTEGER
  );`

export const initializeProjectTable = async function(db: Database) {
  await serialize(db)
  return new Promise<void>(async (resolve, reject) => {
    log('Initialize table project')
    db.run(sql, async err => {
      if (err) reject(err)
      else {
        log('Table project initialized')
        resolve()
      }
    })
  })
}
