import { Database } from 'sqlite3'
import debugFactory from 'debug'

import serialize from '../serialize'
import { ProjectDataStructure } from './types'

const log = debugFactory('app:db:project:delete')

const sql = /* sql */ `
  DELETE FROM project
  WHERE projectID = ?;`

export type DeleteProjectProps = Pick<ProjectDataStructure, 'projectID'>
export type DeleteProjectResult = boolean
export type DeleteProject = (
  db: Database,
  props: DeleteProjectProps
) => Promise<DeleteProjectResult>

export const deleteProject: DeleteProject = async function(db, { projectID }) {
  return new Promise(async (resolve, reject) => {
    log('Delete project %s', projectID)
    await serialize(db)
    db.run(sql, [projectID], async function(err) {
      if (err) reject(err)
      else {
        log('Project %s deleted', projectID)
        resolve(this.changes === 1)
      }
    })
  })
}
