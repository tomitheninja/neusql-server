import { Database } from 'sqlite3'
import debugFactory from 'debug'

import { ProjectDataStructure } from './types'

const log = debugFactory('app:db:project:getOwner')

const sql = /* sql */ `
  SELECT owner
  FROM project
  WHERE projectID = ?;`

type QueryResult = Pick<ProjectDataStructure, 'owner'>

export type GetProjectOwnerProps = Pick<ProjectDataStructure, 'projectID'>

export type GetProjectOwner = (
  db: Database,
  props: GetProjectOwnerProps
) => Promise<ProjectDataStructure['owner']>

export const getProjectOwner: GetProjectOwner = async function(
  db,
  { projectID }
) {
  return new Promise<ProjectDataStructure['owner']>(async (resolve, reject) => {
    log('Get project.owner for %d', projectID)
    db.get(sql, [projectID], async function(err, row: QueryResult) {
      if (err) reject(err)
      else if (row.owner == null) reject(new Error('Invalid owner in database'))
      else {
        log('Got project.owner for %d', projectID)
        resolve(row.owner)
      }
    })
  })
}
