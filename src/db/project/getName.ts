import { Database } from 'sqlite3'
import debugFactory from 'debug'

import { ProjectDataStructure } from './types'

const log = debugFactory('app:db:project:getName')

const sql = /* sql */ `
  SELECT projectName
  FROM project
  WHERE projectID = ?;`

type QueryResult = Pick<ProjectDataStructure, 'projectName'>
export type GetProjectNameProps = Pick<ProjectDataStructure, 'projectID'>
export type GetProjectNameResult = ProjectDataStructure['projectName']

export type GetProjectName = (
  db: Database,
  props: GetProjectNameProps
) => Promise<GetProjectNameResult>

export const getProjectName: GetProjectName = async function(
  db,
  { projectID }
) {
  return new Promise<ProjectDataStructure['projectName']>(
    async (resolve, reject) => {
      log('Get project.name for %d', projectID)
      db.get(sql, [projectID], async function(err, row: QueryResult) {
        if (err) reject(err)
        else {
          log('Got project.name for %d', projectID)
          resolve(row.projectName)
        }
      })
    }
  )
}
