export interface ProjectDataStructure {
  projectID: number
  projectName: string
  createdAt: string
  owner: number // TODO: UserID
}

export type ProjectID = ProjectDataStructure['projectID']
