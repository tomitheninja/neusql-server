export interface WorkspaceLogsDataStructure {
  createdAt: Date
  log: string
}

export type WorkspaceID = number
