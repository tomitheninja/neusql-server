import { Database } from 'sqlite3'
import debugFactory from 'debug'

import { WorkspaceID } from './types'
import serialize from '~db/serialize'

const log = debugFactory('app:db:workspace:run-SQL')

export interface RunSQLinWorkspaceProps {
  workspaceID: WorkspaceID
  sql: string
}

export type RunSQLinWorkspaceResult = {
  rows: unknown[]
}

export type RunSQLinWorkspace = (
  db: Database,
  props: RunSQLinWorkspaceProps
) => Promise<RunSQLinWorkspaceResult>

export const runSQLinWorkspace: RunSQLinWorkspace = async function(
  db,
  { workspaceID, sql }
) {
  return new Promise(async (resolve, reject) => {
    log('Execute SQL in workspace %s', workspaceID)
    await serialize(db)
    db.all(sql, async function(err, rows: unknown[]) {
      if (err) reject(err)
      else {
        log('Executed SQL in workspace %s', workspaceID)
        resolve({ rows })
      }
    })
  })
}
