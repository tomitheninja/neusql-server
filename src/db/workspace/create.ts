import { Database } from 'sqlite3'
import debugFactory from 'debug'

import { WorkspaceID } from './types'
import serialize from '~/db/serialize'

const log = debugFactory('app:db:workspace:create')

const sql = /* sql */ `
  CREATE TABLE _logs (
    createdAt TIME NOT NULL
    log TEXT
  );`

export type CreateWorkspaceProps = {}

export type CreateWorkspace = (
  db: Database,
  props?: CreateWorkspaceProps
) => Promise<WorkspaceID>

export const createWorkspace: CreateWorkspace = async function(db) {
  const sessionID = Math.random() // Used only by the logs
  return new Promise(async (resolve, reject) => {
    log('Create new workspace; session %s', sessionID)
    await serialize(db)
    db.run(sql, { $createdAt: new Date() }, async function(err) {
      if (err) reject(err)
      else {
        log('Workspace created; session %s', sessionID)
        resolve(this.lastID)
      }
    })
  })
}
