import debug from 'debug'
import { Application } from 'express'

const log = debug('app:middleware')

export const mountMiddleware = function(app: Application) {
  log('Mount middleware')
  log('Middleware mounted')
  return app
}
