import supertest from 'supertest'

import { app } from '~/app'

describe('app', () => {
  test('Should export a truthy value', async () => {
    expect(app).toBeTruthy()
  })

  test('Should respond on /', async done => {
    await supertest(app).get('/')
    done()
  })
})
