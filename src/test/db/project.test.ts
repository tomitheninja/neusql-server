import SQLite, { Database } from 'sqlite3'
import * as project from '../../db/project'

describe('db/project', () => {
  let db: Database
  beforeEach(async done => {
    db = new SQLite.Database(':memory:')
    await project.initializeProjectTable(db)
    done()
  })
  afterEach(done => {
    db.close(err => {
      if (err) throw err
      else done()
    })
  })
  test('Can create project', async done => {
    await project.createProject(db, {
      owner: 0,
      projectName: 'test_project_1',
    })
    done()
  })
  test('Can get project name', async done => {
    const projectName = 'test_' + Math.random()
    const projectID = await project.createProject(db, {
      owner: 0,
      projectName,
    })
    const result = await project.getProjectName(db, { projectID })
    expect(result).toBe(projectName)
    done()
  })
  test('Can set project name', async done => {
    const projectID = await project.createProject(db, {
      owner: 0,
      projectName: 'test_initial',
    })
    const projectName = 'test_' + Math.random()
    await project.setProjectName(db, { projectID, projectName })
    const result = await project.getProjectName(db, { projectID })
    expect(result).toBe(projectName)
    done()
  })

  test('Can get project owner', async done => {
    const owner = Math.floor(Math.random() * 10000)
    const projectID = await project.createProject(db, {
      owner,
      projectName: 'test',
    })
    const result = await project.getProjectOwner(db, { projectID })
    expect(result).toBe(owner)
    done()
  })
  test('Can get project createdAt', async done => {
    const now = new Date()
    const projectID = await project.createProject(db, {
      owner: 0,
      projectName: 'test',
    })
    const result = await project.getProjectCreatedAt(db, {
      projectID,
    })

    expect(Math.abs(Number(now) - Number(result))).toBeLessThan(10 * 1000)
    done()
  })
  test('Can list projects', async done => {
    const projectID = await project.createProject(db, {
      owner: 0,
      projectName: 'test',
    })
    const projects = await project.listProjects(db)
    expect(projects).toMatchObject([{ projectID }])
    done()
  })

  test('Can delete project', async done => {
    const projectID = await project.createProject(db, {
      owner: 0,
      projectName: 'test',
    })
    await project.deleteProject(db, { projectID })
    const projects = await project.listProjects(db)
    expect(projects).toMatchObject([])
    done()
  })
})
