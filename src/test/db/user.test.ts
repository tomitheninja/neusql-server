import SQLite, { Database } from 'sqlite3'
import * as user from '../../db/user'

describe('db/user', () => {
  let db: Database
  beforeEach(async done => {
    db = new SQLite.Database(':memory:')
    await user.initializeUserTable(db)
    done()
  })
  afterEach(done => {
    db.close(err => {
      if (err) throw err
      else done()
    })
  })
  test('Can create user', async done => {
    await user.createUser(db, {
      userName: 'test_userName',
      password: 'test_password',
    })
    done()
  })
  test('Can get user name', async done => {
    const userName = 'test_userName_' + Math.random()
    const password = 'test_password_' + Math.random()
    const userID = await user.createUser(db, { userName, password })
    const result = await user.getUserName(db, { userID })
    expect(result).toBe(userName)
    done()
  })
  test('Can set user name', async done => {
    const userID = await user.createUser(db, {
      userName: 'test_initial_username',
      password: 'test_password_' + Math.random(),
    })
    const userName = 'test_' + Math.random()
    await user.setUserName(db, { userID, userName })
    const result = await user.getUserName(db, { userID })
    expect(result).toBe(userName)
    done()
  })
  test('Can get user createdAt', async done => {
    const now = new Date()
    const userName = 'test_userName_' + Math.random()
    const password = 'test_password_' + Math.random()
    const userID = await user.createUser(db, { userName, password })
    const result = await user.getUserCreatedAt(db, { userID })

    expect(Math.abs(Number(now) - Number(result))).toBeLessThan(10 * 1000)
    done()
  })
  test('Can list users', async done => {
    const userName = 'test_userName_' + Math.random()
    const password = 'test_password_' + Math.random()
    const userID = await user.createUser(db, { userName, password })
    const userList = await user.listUsers(db)
    expect(userList).toMatchObject([{ userID }])
    done()
  })

  test('Can delete user', async done => {
    const userName = 'test_userName_' + Math.random()
    const password = 'test_password_' + Math.random()
    const userID = await user.createUser(db, { userName, password })
    await user.deleteUser(db, { userID })
    const users = await user.listUsers(db)
    expect(users).toMatchObject([])
    done()
  })

  test('comparePassword should work with correct password', async done => {
    const initialPassword = 'test_password_' + Math.random()
    const userID = await user.createUser(db, {
      userName: 'test_initial_username',
      password: initialPassword,
    })
    const isMatch = await user.comparePassword(db, {
      userID,
      password: initialPassword,
    })
    expect(isMatch).toBe(true)
    done()
  })

  test('comparePassword should work with incorrect password', async done => {
    const userID = await user.createUser(db, {
      userName: 'test_initial_username',
      password: 'test_password_' + Math.random(),
    })
    const isMatches = await user.comparePassword(db, {
      userID,
      password: 'A_WRONG_PASSWORD',
    })
    expect(isMatches).toBe(false)
    done()
  })

  test('Can change user password', async done => {
    const userID = await user.createUser(db, {
      userName: 'test_initial_username',
      password: 'test_password_old_' + Math.random(),
    })
    const newPassword = 'test_password_new_' + Math.random()
    await user.setUserPassword(db, { userID, password: newPassword })
    const isMatches = await user.comparePassword(db, {
      userID,
      password: newPassword,
    })
    expect(isMatches).toBe(true)
    done()
  })
})
