import { join } from 'path'
import dotEnv from 'dotenv'

async function loadDotEnvConfig(baseDir: string) {
  dotEnv.config({
    path: join(baseDir, '.env.' + process.env.NODE_ENV),
  })
  dotEnv.config({
    path: join(baseDir, '.env'),
  })
}

async function loadDefaultConfig() {
  /**
   * @description Default config - DO NO PLACE SECRETS HERE
   */

  const DEFAULT_CONFIG = {
    DEBUG: 'app:*',
    DEBUG_COLORS: '1',
  }

  process.env = {
    ...DEFAULT_CONFIG,
    ...process.env,
  }
}

async function loadConfig(baseDir: string) {
  await loadDotEnvConfig(baseDir)
  await loadDefaultConfig()
}

const __GLOBAL_THIS__ = this
async function pipeDebugLogsToConsole() {
  require('debug').log = console.log.bind(__GLOBAL_THIS__)
}

async function main() {
  const NODE_ENV = process.env.NODE_ENV || 'development'
  if (NODE_ENV !== 'production') {
    const baseDir = process.env.DOTENV_DIR || join(__dirname, '/..')
    await loadConfig(baseDir)
    await pipeDebugLogsToConsole()
  }

  require('~app').start()
}

main()
