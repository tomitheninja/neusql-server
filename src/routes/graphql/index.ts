import { ApolloServer } from 'apollo-server-express'
import typeDefs from './schema.graphql'
import * as resolvers from './resolvers'

const graphqlServer = new ApolloServer({
  typeDefs,
  resolvers,
})

export { graphqlServer }
