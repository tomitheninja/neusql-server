import debug from 'debug'
import { Application } from 'express'

import { graphqlServer } from './graphql'

const log = debug('app:router')

export const mountRouter = function(app: Application) {
  log('Mount routers')

  log('Mount graphql server')
  graphqlServer.applyMiddleware({ app })
  log('Mounted graphql server')

  log('Mounted routers')
  return app
}
