import { mkdirSync } from 'fs'
import debug from 'debug'

const log = debug('app:config')

log('Load config')

const ENV = process.env

export const NODE_ENV = ENV.NODE_ENV || 'development'
log('NODE_ENV = "%s"', NODE_ENV)

export const PORT = (() => {
  if (NODE_ENV !== 'test' && !ENV.PORT) throw new Error('Missing $PORT')
  return ENV.PORT!
})()

export const DATA_PATH = (() => {
  const dataPath = ENV.DATA_PATH
  if (NODE_ENV === 'test') return null
  if (!dataPath) throw new Error('Missing $DATA_PATH')
  mkdirSync(dataPath, { recursive: true })
  return dataPath
})()

export const JWT_SECRET = (() => {
  const jwtSecret = ENV.JWT_SECRET
  if (NODE_ENV === 'test') return 'env_test_JWT_SECRET'
  if (!jwtSecret) throw new Error('Missing $JWT_SECRET')
  if (jwtSecret.length < 50) {
    throw new Error('$JWT_SECRET should be at least 50 characters')
  }
  return jwtSecret
})()

export const BCRYPT_ROUNDS = (() => {
  if (!ENV.BCRYPT_ROUNDS || NODE_ENV === 'test') return 12
  const bcryptRounds = Number.parseInt(ENV.BCRYPT_ROUNDS, 10)
  if (Number.isNaN(bcryptRounds)) {
    throw new Error('$BCRYPT ROUNDS should be an integer')
  }
  if (bcryptRounds < 10) {
    throw new Error('$BCRYPT ROUNDS should be at least 10')
  }
  return bcryptRounds
})()

log('Loaded config')
