import debug from 'debug'
import express from 'express'
import { PORT } from '~config'

import { mountMiddleware } from '~/middleware'
import { mountRouter } from '~/routes'
import { waitForDatabaseInitialization } from '~/db'

const log = debug('app:')
const serverLog = debug('app:__start__')

log('Create app')
const app = express()

mountMiddleware(app)
mountRouter(app)

log('App created')

const start = async (port = PORT) => {
  serverLog('Await db initialization')
  await waitForDatabaseInitialization()
  serverLog('db initialized')
  serverLog('Start server on port %s', port)
  app.listen(port, () => {
    serverLog('Started server on port %s', port)
    console.log('Server listening on port', port)
  })
}

export { app, start }
